package com.bumer.apiempleo.apiempleo.services;

import com.bumer.apiempleo.apiempleo.model.Empleo;


import java.util.List;


public interface IEmpleoService {

    List<Empleo> findAll() throws Exception;
    Empleo findEmpleadoById(Integer id) throws Exception;
    List<Empleo> findAllEmpleadosByArea(String area) throws Exception;
    Empleo saveEmpresa(Empleo empleo) throws Exception;
    void elimiarEmpleado(Empleo empleo) throws Exception;

}
