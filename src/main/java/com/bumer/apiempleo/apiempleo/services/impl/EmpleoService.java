package com.bumer.apiempleo.apiempleo.services.impl;

import com.bumer.apiempleo.apiempleo.model.Empleo;
import com.bumer.apiempleo.apiempleo.repositories.IEmpleoRepository;
import com.bumer.apiempleo.apiempleo.services.IEmpleoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmpleoService implements IEmpleoService {

    @Autowired
    IEmpleoRepository empleoDAO;

    @Override
    public Empleo saveEmpresa(Empleo empleo) throws Exception {
        return this.empleoDAO.save(empleo);
    }

    @Override
    public void elimiarEmpleado(Empleo empleo) throws Exception {
        this.empleoDAO.delete(empleo);
    }

    @Override
    public Empleo findEmpleadoById(Integer id) throws Exception {
        List<Empleo> list = this.empleoDAO.findAll();
        if(list.size()==0){
            return null;
        }
        return list.stream().filter(empleo -> empleo.getId()==id).collect(Collectors.toList()).get(0);
    }

    @Override
    public List<Empleo> findAllEmpleadosByArea(String area) throws Exception {
        return this.empleoDAO.findAll().stream().filter(empleo -> empleo.getArea().equalsIgnoreCase(area)).collect(Collectors.toList());
    }

    @Override
    public List<Empleo> findAll() throws Exception {
        return this.empleoDAO.findAll();
    }
}
