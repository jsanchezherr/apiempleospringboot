package com.bumer.apiempleo.apiempleo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiEmpleoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiEmpleoApplication.class, args);
	}

}
