package com.bumer.apiempleo.apiempleo.httpService.impl;


import com.bumer.apiempleo.apiempleo.dto.EmpresaDTO;
import com.bumer.apiempleo.apiempleo.httpService.IEmpresaServiceRest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class EmpresaServiceRest implements IEmpresaServiceRest {

    private String urlEmpresa = "http://localhost:5000/api/empresa";

    @Autowired
    RestTemplate restTemplate;

    @Override
    public List<EmpresaDTO> listar() {
        var empresas = this.restTemplate.getForObject(urlEmpresa, EmpresaDTO[].class);
        return Arrays.asList(empresas);
    }

    @Override
    public EmpresaDTO obtenerEmpresa(Integer id) {
        var empresa = this.restTemplate.getForObject(urlEmpresa +"/" +id, EmpresaDTO.class);
        return empresa;
    }




}
