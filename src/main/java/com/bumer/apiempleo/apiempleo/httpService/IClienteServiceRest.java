package com.bumer.apiempleo.apiempleo.httpService;


import com.bumer.apiempleo.apiempleo.dto.ClienteDTO;

import java.util.List;


public interface IClienteServiceRest {
    List<ClienteDTO> listar();

    ClienteDTO obtenerCliente(Integer id);
}
