package com.bumer.apiempleo.apiempleo.httpService.impl;

import com.bumer.apiempleo.apiempleo.dto.ClienteDTO;
import com.bumer.apiempleo.apiempleo.httpService.IClienteServiceRest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class ClienteServiceRest implements IClienteServiceRest {

    private String urlCliente = "http://localhost:5000/api/cliente";

    @Autowired
    RestTemplate restTemplate;

    @Override
    public List<ClienteDTO> listar() {
        var clientes = this.restTemplate.getForObject(urlCliente, ClienteDTO[].class);
        return Arrays.asList(clientes);
    }

    @Override
    public ClienteDTO obtenerCliente(Integer id) {
        var cliente = this.restTemplate.getForObject(urlCliente +"/" +id, ClienteDTO.class);
        return cliente;
    }


}
