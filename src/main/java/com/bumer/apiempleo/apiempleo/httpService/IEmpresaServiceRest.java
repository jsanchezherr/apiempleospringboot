package com.bumer.apiempleo.apiempleo.httpService;

import com.bumer.apiempleo.apiempleo.dto.ClienteDTO;
import com.bumer.apiempleo.apiempleo.dto.EmpresaDTO;

import java.util.List;

public interface IEmpresaServiceRest {

    List<EmpresaDTO> listar();

    EmpresaDTO obtenerEmpresa(Integer id);

}
