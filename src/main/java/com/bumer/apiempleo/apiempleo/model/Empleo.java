package com.bumer.apiempleo.apiempleo.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
//import java.time.LocalDate;


    @Entity
    @Data
    public class Empleo implements Serializable {
        @Id
        @GeneratedValue(strategy= GenerationType.IDENTITY)
        private Integer id;

        @Column(nullable = false, length = 50)
        private String puesto;
        @Column(nullable = false, length = 50)
        private String area;
        @Column(nullable = false, length = 50)
        private Integer cantidadPersona;
        @Column(nullable = false, length = 50)
        private Double salario;
       /* @Column(nullable = false, length = 50)
        private LocalDate fechaPublicacion;
        @Column(nullable = false, length = 50)
        private LocalDate fechaFin;*/
        @Column(nullable = false, length = 50)
        private String departamento;

    }



