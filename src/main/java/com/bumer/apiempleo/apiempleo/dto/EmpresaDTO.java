package com.bumer.apiempleo.apiempleo.dto;

import lombok.Data;



@Data
public class EmpresaDTO {

    private long id;
    private String rubro;
}
