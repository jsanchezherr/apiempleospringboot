package com.bumer.apiempleo.apiempleo.dto;

import com.bumer.apiempleo.apiempleo.model.Empleo;
import lombok.Data;

import java.util.List;

@Data
public class EmpleoDTO {
    Empleo empleo ;
    EmpresaDTO empresa;

}
