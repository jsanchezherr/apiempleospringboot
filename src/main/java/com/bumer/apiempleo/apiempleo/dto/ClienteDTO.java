package com.bumer.apiempleo.apiempleo.dto;

import lombok.Data;

@Data
public class ClienteDTO {

    private Integer id;

    private String nombres;

    private String apellidos;

    private String dni;

}
