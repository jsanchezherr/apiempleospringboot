package com.bumer.apiempleo.apiempleo.dto;

import lombok.Data;

@Data
public class ErroresComunes {
    private String message;
    private String codigo;
}
