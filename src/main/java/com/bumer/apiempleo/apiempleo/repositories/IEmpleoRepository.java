package com.bumer.apiempleo.apiempleo.repositories;

import com.bumer.apiempleo.apiempleo.model.Empleo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IEmpleoRepository extends JpaRepository<Empleo, Integer> {


}
