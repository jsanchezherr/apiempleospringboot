package com.bumer.apiempleo.apiempleo.controller;


import com.bumer.apiempleo.apiempleo.dto.EmpleoDTO;
import com.bumer.apiempleo.apiempleo.dto.EmpresaDTO;
import com.bumer.apiempleo.apiempleo.dto.ErroresComunes;
import com.bumer.apiempleo.apiempleo.httpService.IEmpresaServiceRest;
import com.bumer.apiempleo.apiempleo.model.Empleo;
import com.bumer.apiempleo.apiempleo.services.IEmpleoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("bumer/api/empleo")
@Api(value = "Api empresa", description = "Api de las empresas", tags = {"Api empresa*"})
public class EmpleoController {

    @Autowired
    IEmpresaServiceRest empresaServiceRest;
    @Autowired
    IEmpleoService empleoService;

    @GetMapping("GET/v1/obtener-empleo/{id}")
    //documentacion con SWAGGER
    @ApiOperation(value = "Obtener empleo por ID", notes = "esto una nota", tags = {"Api empleo"})
    @ApiResponses({
            @ApiResponse(code = 200, message = "Esto un 200 OK", response = Empleo[].class),
            @ApiResponse(code = 401, message = "Unauthorized ejem", response = ErroresComunes.class),
            @ApiResponse(code = 403, message = "Forbidden ejem", response = ErroresComunes.class),
            @ApiResponse(code = 404, message = "Not found ejm", response = ErroresComunes.class),
            @ApiResponse(code = 500, message = "Server internal error ejem", response = ErroresComunes.class)
    })
    //end
    public ResponseEntity<Empleo> obtenerEmpleadoPorId(@PathVariable("id") int id) throws Exception{
        return  new ResponseEntity<>(this.empleoService.findEmpleadoById(id), HttpStatus.CREATED);
    }





    @GetMapping("GET/v1/obtener-todos-empleos")
    //documentacion con SWAGGER
    @ApiOperation(value = "Obtener todos empleos ", notes = "esto una nota", tags = {"Api empleo"})
    @ApiResponses({
            @ApiResponse(code = 200, message = "Esto un 200 OK", response = Empleo[].class),
            @ApiResponse(code = 401, message = "Unauthorized ejem", response = ErroresComunes.class),
            @ApiResponse(code = 403, message = "Forbidden ejem", response = ErroresComunes.class),
            @ApiResponse(code = 404, message = "Not found ejm", response = ErroresComunes.class),
            @ApiResponse(code = 500, message = "Server internal error ejem", response = ErroresComunes.class)
    })
    //end
    public ResponseEntity<List<Empleo>> obtenerAllEmpleos() throws Exception{
        return  new ResponseEntity<>(this.empleoService.findAll(),HttpStatus.OK);
    }


    @GetMapping("GET/v1/obtenerEmpleosByArea")
    //documentacion con SWAGGER
    @ApiOperation(value = "Obtener empleo por Area", notes = "esto una nota", tags = {"Api empleo"})
    @ApiResponses({
            @ApiResponse(code = 200, message = "Esto un 200 OK", response = Empleo[].class),
            @ApiResponse(code = 401, message = "Unauthorized ejem", response = ErroresComunes.class),
            @ApiResponse(code = 403, message = "Forbidden ejem", response = ErroresComunes.class),
            @ApiResponse(code = 404, message = "Not found ejm", response = ErroresComunes.class),
            @ApiResponse(code = 500, message = "Server internal error ejem", response = ErroresComunes.class)
    })
    //end
    public ResponseEntity<List<Empleo>> obtenerAllEmpleosByArea(@PathParam("area") String area) throws Exception{
        return  new ResponseEntity<>(this.empleoService.findAllEmpleadosByArea(area),HttpStatus.OK);
    }



    @GetMapping("GET/v1/obtener-empleo/{id}/empresa/{idempresa}")
    //documentacion con SWAGGER
    @ApiOperation(value = "Obtener empleo por ID en la Empresa ID", notes = "esto una nota", tags = {"Api empleo"})
    @ApiResponses({
            @ApiResponse(code = 200, message = "Esto un 200 OK", response = Empleo[].class),
            @ApiResponse(code = 401, message = "Unauthorized ejem", response = ErroresComunes.class),
            @ApiResponse(code = 403, message = "Forbidden ejem", response = ErroresComunes.class),
            @ApiResponse(code = 404, message = "Not found ejm", response = ErroresComunes.class),
            @ApiResponse(code = 500, message = "Server internal error ejem", response = ErroresComunes.class)
    })
    //end
    public ResponseEntity<EmpleoDTO> obtenerEmpleoPorId(@PathVariable("id") int id, @PathVariable("idempresa") int idEmpresa) throws Exception{

        try{
            EmpleoDTO empleoDTO = new EmpleoDTO();
            empleoDTO.setEmpleo(Optional.ofNullable(empleoService.findEmpleadoById(id)).orElse(new Empleo()));
            empleoDTO.setEmpresa(Optional.ofNullable( empresaServiceRest.obtenerEmpresa(idEmpresa)).orElse( new EmpresaDTO()));

            return  new ResponseEntity<EmpleoDTO>(empleoDTO,HttpStatus.CREATED);
        }catch (Exception err){
            return  new ResponseEntity<EmpleoDTO>(new EmpleoDTO(),HttpStatus.BAD_REQUEST);
        }

    }




}
